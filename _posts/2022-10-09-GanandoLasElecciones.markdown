---
layout: post
title: "Ganando las elecciones"
date: 2022-10-09 16:07:58 +0200
categories: kata
---
## **Ganando las elecciones**

¡Estamos en elecciones! Teniendo en cuenta el número de votos recibidos por cada candidatura y el número de sufragados que aún no han votado, calcula el número de candidaturas que aún tienen posibilidades de ganar. Kata basada en: https://www.codewars.com/kata/simple-fun-number-41-elections-winners


### Número de votos

El número de votos puede representarse como un array de valores:
-   Votos por candidatura: \[2, 5, 7, 0, 1\]

    Significa que la primera candidatura ha conseguido 2 votos, la segunda 5, la tercera 7, etc


### Voto indeciso

El número de votos indecisos es un entero |

### Ganadora

La ganadora debe tener estrictamente más votos que cualquier otra candidatura |

### Repetición de elecciones

Un empate de votos máximos significa que no hay ninguna ganadora |

### Ejemplos

Votos: \[3, 7\]
Indecisos: 1

Si todos los votos indecisos van a la primera candidatura consigue 4 votos.

Si todos los votos indecisos van a la segunda candidatura consigue 8 votos

Resultado: Hay una candidatura con posibilidades de ganar

Votos: \[3, 7, 2, 6, 8\]
Indecisos: 3

Si al menos 2 votos indecisos van a la segunda candidatura y ninguno a la quinta, gana las elecciones.

Si todos los votos indecisos van a la cuarta candidatura, gana las elecciones

En cualquier otro caso, la quinta candidatura gana las elecciones

Resultado: Hay 3 candidaturas con posibilidades de ganar las elecciones
