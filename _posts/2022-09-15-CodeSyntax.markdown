---
layout: post
title: "Code Syntax"
date: 2022-09-15 16:07:58 +0200
categories: kata
---
## **Code Syntax**

En un futuro no muy lejano… pero tampoco muy cercano ;) La humanidad ha creado un lenguaje de programación cuántico avanzado, es tan potente que puede llegar a crear un virus informático capaz de acabar con el malvado ordenador conocido como Skynet y así salvar a la humanidad… Pero el lenguaje de programación tiene unos pequeños inconvenientes: Se puede ejecutar muy pocas veces, ya que cada vez que se enciende Skynet lo detecta y se aproxima a él. Es muy, pero muy complejo de escribir.
El lenguaje se escribe usando solo 2 símbolos, que son el “<” y el “>”. A nosotros nos ha tocado desarrollar un “linter” o herramienta que nos diga que todos los “<” en algún momento se cierra con “>”. Como se puede encender poco, así nos aseguramos que no habrá errores de sintaxis.
Voy a escribir algún programa básico para que te hagas una idea de que es una buena sistaxis.:

- El típico hola mundo sería: `<>`
- Algo más serio: `<<><>><>`

Los errores más comunes:
- `<`
- `>`
- `<<`
- `>>`
- `><`
- `><><<`

TIP: Para resolver esta kata y no morir en el intento, se tiene que pensar en cuál es el paso más pequeñito que se puede dar, cuál sería el test que menos código nos hiciera escribir.

!PROBLEMAS! Skynet ha evolucionado y ahora es capaz de enfrentarse al virus que se había diseñado. Para solucionar esta crisis, los ingenieros han decidido añadir dos símbolos más, para que sea el doble de potente. Ahora también acepta “[“ y “]” Ahora se pueden crear programas como:

- `[<><>[]]`
- `<>[]<>[]`
- `<[<>]>`

Pero cuidado, que no se aceptan cosas como:

- `<[><]>`
- `[<][>]`

SUERTE!
