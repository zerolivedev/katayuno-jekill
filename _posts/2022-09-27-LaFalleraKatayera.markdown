---
layout: post
title: "La Fallera Katayera"
date: 2022-09-27 16:07:58 +0200
categories: kata
---
## **La Fallera Katayera**

Una fallera se encontraba programando en La Cova cuando de repente el estruendo de un petardo la asustó. Sobresaltada, soltó la taza de entre sus manos, cayendo todo el café sobre el portátil y provocando un cortocircuito que la electrocutó! Desde entonces la Fallera Katayera acecha los Katayunos de la Devscola. Algunos la han visto por el rabillo del ojo, o reflejada en la pantalla del portátil. Incluso una chica afirma haber paireado con ella después de su muerte! Para apaciguar el dolor de su alma en pena y devolverla al otro mundo, tenemos que hacerle una buena paella!

### Introducción al juego

- El objetivo del juego es crear una paella que guste a la Fallera Katayera
- Cada jugador empieza con 7 cartas, y gana el que antes consiga reunir los 5 ingredientes necesarios

### Las cartas

Existen distintos tipos de carta:
- **Ingrediente:** Un ingrediente "lícito" de la paella valenciana (Pollo, conejo, tomate, alcachofa, habas, judías verdes...)
- **Monstruo:** Los monstruos tienen puntos de ataque.

### La mecánica

En su turno, el jugador puede realizar 2 acciones:
- **Atacar:** Utilizar una carta de monstruo para atacar a otro jugador con el objetivo de robarle un ingrediente. El ataque tiene distintos finales:
    1.  **Ambos jugadores empatan en puntos de ataque:** El defensor conserva sus ingredientes
    2.  **El atacante supera en puntos de ataque al defensor:** El atacante roba un ingrediente al defensor
    3.  **El defensor supera en puntos de ataque al atacante:** El defensor conserva sus ingredientes
- **Robar:** Robar una carta del mazo

### Extra

Podéis adaptar este juego como queráis. Las reglas descritas son sólo una guía, pero sentíos libres de añadir nuevas reglas o modificar las existentes. Si algo no se entiende bien, llegad a un acuerdo
Esta kata esta basada en el juego de cartas [La Fallera Calavera](https://zombipaella.com/la-fallera-calavera)
