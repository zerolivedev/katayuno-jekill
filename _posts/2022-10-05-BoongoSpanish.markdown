---
layout: post
title: "Boongo (Spanish)"
date: 2022-10-05 16:07:58 +0200
categories: kata
---
## Boongo (Spanish)

¡El bingo más terrorífico! Incluye personajes horripilantes en los cartones, y desagradables sorpresas en el bombo!

### El cartón

Cada jugador tiene un cartón con personajes de Halloween como el de la imagen. El bombo gira y de él van saliendo personajes:

\['vampiro', 'bruja', 'fantasma', 'hombre lobo'\]

Cada jugador marca el personaje que ha salido del bombo en caso de que aparezca en su cartón.

Cuando un jugador "hace línea", es decir, marca todos los personajes de una misma fila, grita "¡Boongo!" y se lleva el bote.

### El bote

Cada jugador debe meter una chuche en la calabaza, y el que gana se lleva todas las chuches.

### El Comecocos (Monstruo especial)

Los monstruos especiales están en el bombo pero no en los cartones, y sólo salen del bombo para ejercer su maldad.

El comecocos se infiltra en la calabaza y se come una chuche por turno hasta que alguien consigue marcar 2 personajes colindantes.

### Gatulu (Monstruo especial)

Cuando el temible gato-pulpo interestelar es despertado de su profundo sueño, borra las marcas de todos los cartones.
