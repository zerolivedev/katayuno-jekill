---
layout: post
title: "Un Pomodoro en un pomodoro"
date: 2022-10-03 16:07:58 +0200
categories: kata
---
## **Un Pomodoro en un pomodoro**

La **Técnica Pomodoro** es un método para mejorar la administración del tiempo dedicado a una actividad. Fue desarrollado por **Francesco Cirillo** a fines de la década de 1980. Se usa un temporizador para dividir el tiempo en intervalos indivisibles, llamados _pomodoros_, de 25 minutos de actividad, seguidos de 5 minutos de descanso, con pausas más largas cada cuatro pomodoros.

### Mecánica básica

1.  Decidir la tarea o actividad a realizar
2.  Poner el temporizador
3.  Trabajar en la tarea de manera intensiva hasta que el temporizador suene
4.  Hacer una marca para anotar qué pomodoro se ha completado
5.  Tomar una pausa breve
6.  Cada cuatro pomodoros, tomar una pausa más larga

Esta cata es la que se uso en el primer Coding Dojo en España el 22/12/2009 en Madrid que fue facilitada por Xavi Gost y José Manuel Beas, podeis ver la convocatoria haciendo click [aquí.](https://www.adictosaltrabajo.com/2009/12/11/el-proximo-dia-22-a-las-1930-vamos-a-celebrar-el-primer-coding-dojo-organizado-por-agilismo-es/)

### Creación

-   Un pomodoro dura 25 minutos por defecto.
-   Se puede crear un pomodoro con cualquier duración.
-   Si se le indica una duración negativa, la duración es 25 minutos.
-   Se pueden crear pomodoros con duración de Zero minutos.
-   El pomodoro no se puede crear con valores nulos.

### Start and coundown

-   El pomodoro empieza parado.
-   El pomodoro lo tiene que arrancar el usuario.
-   Al arrancar el pomodoro empieza la cuenta atrás.

### END

-   Un pomodoro no termina si no ha sido arrancado.
-   Un pomodoro acaba cuando se agota el tiempo.
-   Un pomodoro no acaba mientras no se agote su tiempo.

### Interruptions

-   Un pomodoro se inicia sin interrupciones.
-   Si el pomodoro no esta iniciado, no se puede interrumpir.
-   El pomodoro cuenta las interrupciones (1, 2...)

### Restart

-   Un pomodoro arrancado se resetea al arrancarlo de nuevo.
-   Un pomodoro se resetea sin interrupciones.
