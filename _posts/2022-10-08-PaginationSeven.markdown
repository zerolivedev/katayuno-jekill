---
layout: post
title: "Pagination Seven"
date: 2022-10-08 16:07:58 +0200
categories: kata
---
## **Pagination Seven**

La páginacion es un elemento que podemos encontrar en muchisimas páginas web, en este elemento se suele ver la página actual que nos encontramos y pasar a la siguiente y la anterior página (si se puede). Aunque hay muchos tipos, como:

Simple: **< 42 >**

Avanzada: **<< < Page 42 of 100 > >>**

Y la nuestra, la de siete elementos:

**1 … 41 (42) 43 … 100**

Intenta resolver los problemas de los cinturones uno a uno, pero tener siempre en cuenta el resto de cinturones.

### Página actual

Indicar **siempre** la página en la que nos encontramos, ejemplos:

Te encuentras en la pagina 2 de 7: **1 (2) 3 4 5 6 7**

Te encuentras en la pagina 5 de 7: **1 2 3 4 (5) 6 7**

### Numero de páginas dinámicos

Ni siempre en todas partes tienen el mismo numero de elementos, por lo tanto no tienen el mismo numero de páginas:

Con 5 paginas: **1 (2) 3 4 5**

Con 7 paginas: **1 (2) 3 4 5 6 7**

### Nunca mostrar mas de 7 elementos

Como el nombre de esta paginación indica solo se tienen que mostrar un máximo de 7 elmentos, ejemplos:

1 ... 4 (5) 6 ... 9

1 ... 49 (50) 51 ... 100

 |

### Acantilados moher irlanda

1 2 3 (4) 5 .... 199

(1) 2 3 4 5 .... 199

1 .... 195 (196) 197 198 199

1 2 3 4 5 .... (199)
