---
layout: post
title: "Scuba Divers"
date: 2022-09-23 16:07:58 +0200
categories: kata
---
## **Scuba Divers**

Los viernes en la Devscola los frikis (con orgullo) se juntan para jugar a juegos de mesa. Este mes han adquirido un nuevo juego llamado Scuba Divers. Después del primer viernes **Jaume** no pudo soportar perder durante toda la noche y decidió hacer un simulador para poder practicar por su cuenta y no seguir perdiendo. Las normas que hay que seguir son las siguientes.

### Preparación del tablero

- El buceador empieza con 150 litros de oxígeno
- El buceador empieza al nivel del mar (Zero metros)

### Acciones por turno

- Puedes descender un metro
- Puedes mantenerte a la profundidad en la que te encuentras
- Puedes ascender un metro
- No puedes ascender por encima del nivel del mar

### Reglas

- Cuando estás bajo la superficie, consumes oxígeno.
- Cuando asciendes, gastas un litro de oxígeno
- Cuando desciendes, gastas el doble de oxígeno que metros que te encuentras al final de la acción
- Cuando te mantienes a la misma profundidad, gastas medio litro de oxígeno
- Cuando te quedas sin oxígeno bajo la superficie, mueres

### Puntuaciones

- Solo puntúas si no mueres
- Obtienes tantos puntos como profundidad a la que te encuentras al final del turno
