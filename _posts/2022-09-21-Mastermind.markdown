---
layout: post
title: "Mastermind"
date: 2022-09-21 16:07:58 +0200
categories: kata
---
## **Mastermind**

¿Alguna vez has jugado Mastermind? Juego donde un creador de códigos, tiene que elegir una combinación secreta de bolas de colores y luego un descifrador de códigos tiene que adivinar esa combinación. El creador de códigos responde a cada intento del descifrador de códigos, indicando solo la cantidad de colores bien posicionados y la cantidad de colores correctos pero fuera de su posición.

Si recuerdas haber jugado el juego, ser el que adivina requiere mucho cerebro, mientras que el otro jugador se aburre rápidamente.

[Kata original](https://codingdojo.org/kata/Mastermind/)

### Descripción del problema

La idea de esta Kata es crear un software capaz de desempeñar el papel aburrido: responder a la cantidad de colores bien colocados y fuera de lugar.

Por lo tanto, su función seria que devolviera, para una combinación secreta y un intento de combinación:

- La cantidad de colores bien colocados.
- La cantidad de colores correctos pero fuera de lugar.

Una combinación puede contener cualquier número de clavijas, pero es mejor que le dé el mismo número para el secreto y las adivinanzas. Puedes usar cualquier cantidad de colores.
