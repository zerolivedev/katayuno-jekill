---
layout: post
title: "Christmas Party!"
date: 2022-09-28 16:07:58 +0200
categories: kata
---
## **Christmas Party!**

How can you tame a wild (and changing) set of Christmas wishes?

Imagine you’re writing a wish processing application for Santa Claus. In the past, Santa used a fairly random mixture of manual and ad-hoc automated practices to handle children wishes; he now wants to put all these various ways of handling wishes together into one whole: your application. However, he (and the children all around the world) has come to cherish the diversity of their wishes rules, and so he tells you that you’ll have to bring all these rules forward into the new system.

When you go in to meet the existing wish entry folks, you discover that their wishes practices border on chaotic: no two wish lines have the same set of processing rules. To make it worse, most of the rules aren’t written down: you’re often told something like “oh, Carol, the elf, on the chocolate floor handles that kind of wish.”

During first day of meetings, you’ve decided to focus on gifts, and in particular on the processing required when a gift was confirmed by Santa, because you know that Santa can reject wishes if they are not good children... You come home, exhausted, with a legal pad full of rule snippets such as:

-   If the gift is for a physical product, generate a packing slip for shipping.
-   If the gift is for a book, create a duplicate packing slip for the Good Children's department, since you know, good boys and girls read books...
-   If the gift is for a Netflix membership, activate that membership.
-   If the gift is an upgrade to a Netflix membership, apply the upgrade.
-   If the gift is for a Netflix membership or upgrade, e-mail the owner and inform them of the activation/upgrade.
-   If the gift is for the dron “The definitive DRON by ACME Tech” add a free “First Aid” kit to the packing slip (the result of a court decision in 2017).
-   If the gift is for a physical product or a book, generate a commission gift to their parents.
-   And so on, and so on, for seven long, long, yellow pages...
-   And each day, to your horror, you gather more and more pages of these rules.
-   Now you’re faced with implementing this system. The rules are complicated, and fairly arbitrary. What’s more, you know that they’re going to change: once the system goes live, all sorts of special cases will come out of the woodwork.

### Objectives

How can you tame these wild wish rules?

How can you build a system that will be flexible enough to handle both the complexity and the need for change?

And how can you do it without condemning yourself to years and years of mindless support, specially on Christmas?
