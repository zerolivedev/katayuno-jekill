---
layout: post
title: "El juego de la oca"
date: 2022-09-20 16:07:58 +0200
categories: kata
---
## **El juego de la oca**

El juego de la oca es un juego en el que dos o más jugadores mueven piezas alrededor de una tablero tirando un dado. El objetivo del juego es alcanzar el cuadrado número sesenta y tres antes que cualquiera de los otros jugadores y evitar obstáculos. Tu misión es escribir un código al que le pasemos las ordenes del juego, y el vaya jugando y dandonos nuestra posicion en el tablero.
Reglas:

- Puedes añadir jugadores: Si no hay jugador, si le pasamos: "add player Sara" , obtenemos: "players: Sara" Si le pasamos: "add player Juan", obtenemos: "players: Sara, Juan"
- Sabe si un jugador esta duplicado: si le pasamos: "add player Juan", obtenemos: "Juan: already existing player"
- Podemos mover un jugador: Si hay dos jugadores "Sara" y "Juan" en la casilla de salida: Si le pasamos: "move Sara 4, 2", obtenemos: "Sara rolls 4, 2. Sara moves from Start to 6" (4 y 2 emulan el valor de la tirada de cada dado) Si le pasamos: "move Juan 2, 2", obtenemos: "Juan rolls 2, 2. Juan moves from Start to 4"
Si le pasamos: "move Sara 2, 3", obtenemos: "Sara rolls 2, 3. Sara moves from 6 to 11"
- Ganamos si llegamos a la casilla 63: Si el jugador "Juan" esta en la casilla "60", Si le pasamos: "move Juan 1, 2", obtenemos: "Juan rolls 1, 2. Juan moves from 60 to 63. Juan Wins!!"
- Solo ganamos si llegamos a la casilla 63 con la tirada exacta (hay rebote): Si el jugador "Juan" esta en la casilla "60", Si le pasamos: "move Juan 3, 2", obtenemos: "Juan rolls 3, 2. Juan moves from 60 to 63. Juan bounces! Juan returns to 61"

### Juego básico

Implementa las reglas básicas descritas arriba.

### El puente

Si un jugador cae en la casilla de "El puente", salta a la casilla "12":
Si la jugadora Sara esta en la casilla "4", y le pasamos: "move Sara 1, 1", obtenemos: "Sara rolls 1, 1. Sara moves from 4 to The Bridge. Sara jumps to 12" |

### Dados automaticos

Vamos a automatizar la tirada de dados:
Si el jugados "Juan" esta en la casilla "4", le pasamos: "move Juan"
El codigo hara una tirada automatica de dados (ninguno de los dos valores podra ser superior a 12 ni inferior a 1)
obtenemos: "Juan rolls 1, 2. Juan moves from 4 to 7" (entiendiendo que de forma aleatoria hemos obtenido una tirada de 1 y 2).
