---
layout: post
title: "Procesador de textos"
date: 2022-10-04 16:07:58 +0200
categories: kata
---
## **Procesador de textos**

Nos han pedido ayuda para contribuir a un procesador de textos de código abierto. Hay varios equipos añadiendo funcionalidades en el repositorio y a nosotros nos han pedido que colaboremos con la parte de búsqueda de palabras.
Las funcionalidades que nos han pedido son:

### Búsqueda de una palabra

-   Buscar si una palabra está contenida en un texto.
-   Saber cuántas veces aparece esa palabra en el texto.

### Palabra más usada

-   Devolver cual es la palabra más usada
-   Sacar la estadística de la palabra más usada ( de 100 palabras la palabra ‘patata’ aparece 5 veces, es decir un 5%)

### Palabras en el texto

-   Contar cuantas veces aparece cada palabra de un texto
-   Devolver las palabras ordenadas por candidad de veces usadas (De mayor cantidad a menor, sin repetir ninguna)
