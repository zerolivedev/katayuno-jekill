---
layout: post
title: "Lista simple"
date: 2022-09-18 16:07:58 +0200
categories: kata
---
## **Lista simple**

Las listas son una de las primeras estructuras de datos que aprendemos como programadores. Pero la familiaridad no significa que no podamos aprender un poco de ellas.

Para esta kata vamos a implementar una lista que tiene la siguiente interfaz básica:

- La lista consta de posiciones. Cada posición tiene un valor de cadena de texto.
- Los nuevos valores se agregan al final de la lista.
- Puede preguntar a la lista si contiene una cadena de texto dada. Si lo hace, devuelve la posición que contiene esa cadena.
- Puede eliminar una posición de la lista.
- A la lista se le puede pedir que se compare con una Array y sepa decirnos si es igual o no.

[Fuente original](http://codekata.com/kata/kata21-simple-lists/)

### Prohibido usar listas!!!

Pues eso, para esta kata esta prohibido usar listas, diccionarios o cualquier iterador que os provea el lenguaje de programación que estáis usando.
