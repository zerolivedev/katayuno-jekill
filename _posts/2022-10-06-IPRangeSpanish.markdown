---
layout: post
title: "IP-Range (Spanish)"
date: 2022-10-06 16:07:58 +0200
categories: kata
---
## **IP-Range**

Este ejercicio consiste en crear una librería que nos calcule un rango de direcciones IP a partir de una dirección IP y con una dirección IP final o una mascara o una mascara corta.

Una dirección IP es una manera de identificar un dispositivo dentro de una red que utilice el protocolo IP. Una dirección IP esta formada por 4 grupos de números que van desde el 0 al 255 y están separados por puntos (192.168.0.1)

\- Una dirección IP final es lo mismo que una dirección IP, pero que es mayor o menor que la dirección IP. Ejemplo:
IP: 172.1.1.1
Final (Mayor) 172.1.1.244
Final (Menor) 172.1.0.1

\- Una mascara tiene el mismo formato que una dirección IP, pero nos indica una red o rango de direcciones IP para una dirección. Ejemplo:
IP: 192.168.0.1
MASK: 255.255.255.0
RANGE\_FROM: 192.168.0.1
RANGE\_TO: 192.168.0.255

\- Una mascara corta es una representación de la mascara pero contando el numero de bits, es un numero que estra entre 1 y 32. Ejemplo:
SHORT\_MASK: 24
LONG\_MASK: 255.255.255.0
In bits: 11111111.11111111.11111111.00000000
Los tres son lo mismo.

Nota: Existen ya muchas librerías que hacen esto y muchos lenguajes de programación las tienen incluidas. Si decides utilizar una de estas librerías crea tus tests y cuando veas que cumples todas las condiciones, intenta cambiar de librería para ver cuanto te cuesta, si has encapsulado la librería correctamente no debería de suponer mucho esfuerzo cambiarla y tus tests seguirán en verde sin cambiarlos.

### Rango por IP final

**Ejemplos:**

INPUT:
ip\_from: 192.168.1.1
ip\_to: 192.168.1.255

OUTPUT
from: 192.168.1.1
to: 192.168.1.255
___

INPUT:
ip\_from: 192.168.1.1
ip\_to: 192.168.0.1

OUTPUT
from: 192.168.0.1
to: 192.168.1.1

### Rango por mascara corta

**Ejemplos:**

INPUT:
ip\_from: 192.168.1.1
short: 24

OUTPUT
from: 192.168.1.1
to: 192.168.1.255
___

INPUT:
ip\_from: 192.168.1.1
short: 16

OUTPUT
from: 192.168.1.1
to: 192.168.255.255

### Rango por mascara

**Ejemplos:**

INPUT:
ip\_from: 192.168.1.1
mask: 255.255.255.0

OUTPUT
from: 192.168.1.1
to: 192.168.1.255
___

INPUT:
ip\_from: 192.168.1.1
mask: 255.255.0.0

OUTPUT
from: 192.168.1.1
to: 192.168.255.255
