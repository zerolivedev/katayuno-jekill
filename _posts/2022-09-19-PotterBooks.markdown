---
layout: post
title: "Potter books"
date: 2022-09-19 16:07:58 +0200
categories: kata
---
## **Potter books**

Para tratar de mejorar las ventas de los 5 libros de Harry Potter que todavia le quedan en stock a una librería, han decidido ofrecer descuentos en compras de libros múltiples. Las ofertas son:

- Una copia de cualquiera de los cinco libros cuesta 8 EUR.
- Sin embargo, si compras dos libros diferentes, obtienes un 5% descuento en esos dos libros.
- Si compras 3 libros diferentes, obtienes un 10% de descuento.
- Si compras 4 libros diferentes, obtienes un 20% de descuento.
- Si quieres el lote completo y compras los 5, obtienes un 25% de descuento.

Ten en cuenta que si compras, por ejemplo, cuatro libros, de los cuales 3 son diferentes títulos, obtienes un 10% de descuento en los 3 que forman parte de un conjunto, pero el cuarto libro todavía cuesta 8 euros.
Tu misión es escribir un código para calcular el precio de cualquier cesta de la compra concebible (que contiene solo Libros de Harry Potter), dando el mayor descuento posible. Ejemplo: Una cesta de la compra con:

2 copias del primer libro

2 copias del segundo libro

2 copias del tercer libro

1 copia del cuarto libro

1 copia del quinto libro

Su precio seria: 5 libros, uno de cada (40\*0.75) más 3 libros, uno de cada (24\*0.90) = 30 + 21.60 = 51.60 €

### Aplicar descuentos

Sin preocuparte por intentar dar el mayor descuento posible, calcula los descuentos por el orden en que se aplican.

### Aplicar el mejor descuento posible

Calcular el precio de cualquier cesta de la compra concebible, dando el mayor descuento posible, como se decia originalmente.

### Incluyendo todos los libros

Ahora añadimos las reglas para los 7 libros de Harry Potter:
- Una copia de cualquiera de los siete libros cuesta 8 EUR.
- Si compras dos libros diferentes, obtienes un 5% descuento en esos dos libros.
- Si compras 3 libros diferentes, obtienes un 10% de descuento.
- Si compras 4 libros diferentes, obtienes un 20% de descuento.
- Si compras 5 libros diferentes, obtienes un 30% de descuento.
- Si compras 6 libros diferentes, obtienes un 40% de descuento.
- Si quieres el lote completo y compras los 7, obtienes un 50% de descuento.

Calcular el precio de cualquier cesta de la compra concebible, dando el mayor descuento posible. |
