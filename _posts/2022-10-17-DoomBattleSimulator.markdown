---
layout: post
title: "DooM Battle Simulator"
date: 2022-10-17 16:07:58 +0200
categories: kata
---
## **DooM Battle Simulator**

The kata was designed for the Devscola in the course of 2017.

This kata is designed to practice the inheritance and testing of random situations.

Doom is a 1993 science fiction horror-themed first-person shooter (FPS) video game by id Software. It is considered one of the most significant and influential titles in video game history, for having helped to pioneer the now-ubiquitous first-person shooter.

Create a battle simulator to help the DooM Marine to know in which situations he could survive and in which he could not survive.

Links to kata description:

-   [English version](https://zerolive.github.io/doom_kata/)
-   [Spanish version](https://zerolive.github.io/doom_kata/index_es.html)
