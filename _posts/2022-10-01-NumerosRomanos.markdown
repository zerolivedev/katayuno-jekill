---
layout: post
title: "Números romanos"
date: 2022-10-01 16:07:58 +0200
categories: kata
---
## **Números romanos**

Los romanos conquistaron la mayor parte de Europa y la gobernaron durante cientos de años, pero nunca descubrieron el número cero. Esto hizo que escribir y fechar historias extensas de sus hazañas fuera un poco más complicado, pero el sistema de números que se les ocurrió todavía está en uso hoy en día. Por ejemplo, algunos programas de television o películas, se fechan con números romanos. Los romanos escribieron números usando letras: I, V, X, L, C, D, M:

1 --> I

8 --> VIII

14 --> XIV

67 --> LXVII

1992 --> MCMXCII

[Ejemplo del uso de los números romanos](https://www.smartick.es/blog/matematicas/recursos-didacticos/los-numeros-romanos/)
**Necesitamos un conversor de números arabigos a números romanos**

### Primer cinturón

Convertir cualquier número entre 1 y 1000 a números romanos

### Segundo cinturón

Convertir cualquier número a números romanos

### Tercer cinturón

Convertir de números romanos a normales
