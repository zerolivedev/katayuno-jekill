FROM ruby:3.1.1

WORKDIR /opt/jekill

COPY Gemfile* ./

RUN bundle install

CMD ["bundle", "exec", "jekyll", "serve"]
