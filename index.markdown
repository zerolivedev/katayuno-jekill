---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
Katayuno (Katas + Desayuno) is a weekly open event where to pair code katas to learn TDD while enjoying a freshly baked breakfast.

If you’d like to know more about Katayuno, check out this article.

### **The kata**

A code kata is a small exercise in programming that is meant to help us programmers sharpen our skills through practice.

### **The TDD**

TDD is a software development process that uses tests to make the design emerge by following the red/green/refactor cycle.

### **The pairing**

Pair programming is an agile software development technique in which two programmers work together at one workstation.

### **The Language**

You can make katas in every languages, the important point is to learn tdd, good practices and pairing.

### **The meetup**

The [meetup](https://www.meetup.com/es-ES/Aprende-a-programar-en-Valencia/events/242889981/) is organized by [Devscola](http://www.devscola.org/).

## **Kata-logue**

Find below all the katas:
